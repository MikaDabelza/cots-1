from django.shortcuts import render
from django.http import JsonResponse
from django.utils.decorators import method_decorator

from django.views.decorators.csrf import csrf_exempt
@method_decorator(csrf_exempt, name='dispatch')
# Create your views here.
def index(request):
    if request.method == "POST":
        var1 = int(request.POST.get("var1"))
        var2 = int(request.POST.get("var2"))
        operator = request.POST.get("operator")
        res = eval(f'{var1}{operator}{var2}')
        return JsonResponse({'res':res})
    return render(request, 'index/index.html')